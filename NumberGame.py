# Created by Sean Kuehlhorn
# Math Puzzle Game

from graphics import *
import random

def makeIntro(win): #Instructing players on on how to play
    rules = Text(Point(250,250), 'In this number game you and your opponent will be given the same 6 singe digit numbers.\n\n'
                                 'You will use them in combination with four arithmetic functions\n'
                                 'to make a number as close or equal to the target number.\n\n'
                                 'Inputs you may use other than the 6 random terms are: + , - , * , / , ( , ).\n\n'
                                 'Any errors in your input will result in a \'0\' score for that round.\n\n'
                                 'The player closer to the target number scores.\n\n'
                                 'Play repeats until a player reaches 3 points! Good Luck!')
    rules.setStyle('bold')
    rules.draw(win)
    intro = Text(Point(250,50), 'Click to begin a two player game.')
    intro.draw(win)
    win.getMouse()
    rules.undraw()
    intro.undraw()

def playerReady(win,x): #Score recap and player new round preparation phase
    if x == '1':
        newRoundPrompt = Text(Point(250,250),'The score is\nPayer 1: ' + str(total1) + '\nPlayer 2: ' + str(total2) + '\n\n'
                                             'This is a new round.\n'
                                             'Play will continue until a player makes 3 points.')
        newRoundPrompt.setStyle('bold')
        newRoundPrompt.setSize(20)
        newRoundPrompt.draw(win)
    else:
        newRoundPrompt = Text(Point(250,250),'Alright Player 2, it is your turn!')
        newRoundPrompt.setStyle('bold')
        newRoundPrompt.setSize(25)
        newRoundPrompt.draw(win)
    prompt = Text(Point(250,50),'Player ' + x + ', click to start your puzzle.\n')
    if x == '1':
        prompt.setFill('red')
    else:
        prompt.setFill('blue')
    prompt.draw(win)
    win.getMouse()
    newRoundPrompt.undraw()
    prompt.undraw()

def playerWin(win,x): #Player win screen
    prompt = Text(Point(250,250), 'Player '+ x + ' wins!')
    prompt.setSize(25)
    prompt.setStyle('bold italic')
    prompt.draw(win)
    win.promptClose(win.getWidth()/2, 190)

def gameBoard(win,x,y,z): #Player game board: target number, terms and entry box appear and gameplay happens
    targetIdentifier = Text(Point(250,430),'Target Number')
    targetIdentifier.setSize(20)
    targetIdentifier.setFill('red')
    targetIdentifier.draw(win)
    target = Text(Point(250, 400),x)
    target.setSize(35)
    target.setStyle('bold')
    target.draw(win)
    termIdentifier = Text(Point(250,330), 'Terms Available')
    termIdentifier.setSize(20)
    termIdentifier.setFill('blue')
    termIdentifier.draw(win)
    terms = Text(Point(250, 300),y)
    terms.setSize(25)
    terms.draw(win)
    entryIdentifier = Text(Point(250,230), 'Enter Your Equation Below, then click anywhere')
    entryIdentifier.setSize(13)
    entryIdentifier.draw(win)
    entry1 = Entry(Point(250, 200),30)
    entry1.setText('0')
    entry1.setSize(15)
    entry1.draw(win)
    win.getMouse()

    if userTermChecker(str(entry1.getText()), str(y)) == 1:
        entry1.setText('Input error, answer is set to 0')
        solution = 0
    else:
        solution = eval(entry1.getText())

    answerIdentifier = Text(Point(250,101),'Your answer is\n\n\nYou are\n\n\naway from the target.')
    answerIdentifier.setSize(20)
    answerIdentifier.draw(win)
    answer = Text(Point(250, 140),solution)
    answer.setSize(36)
    answer.setFill('purple')
    answer.draw(win)
    away = Text(Point(250, 65),abs(x-solution))
    away.setSize(28)
    away.setFill('red')
    away.draw(win)
    win.getMouse()
    targetIdentifier.undraw()
    target.undraw()
    termIdentifier.undraw()
    terms.undraw()
    entryIdentifier.undraw()
    entry1.undraw()
    answerIdentifier.undraw()
    answer.undraw()
    away.undraw()
    return (abs(x-solution), entry1.getText())

def roundSummary(win,x,y,x1,y1): #Players inputs are compared side by side and a point is visually awarded to the round winner
    summary = Text(Point(250,400),'Round Recap')
    summary.setFill('purple')
    summary.setSize(30)
    summary.draw(win)
    userInputs = Text(Point(250,250),'Player 1 did: ' + str(x1) + '\nand was ' + str(x) + ' away\n\n'
                                     'Player 2 did: ' + str(y1) + '\nand was ' + str(y) + ' away.')
    userInputs.setSize(25)
    userInputs.draw(win)
    proceed = Text(Point(250,50),'Click anywhere to continue.')
    proceed.draw(win)

    if x < y:
        resultOutput = Text(Point(250,100),'Player 1 scores.')
        resultOutput.setFill('red')
    elif y < x:
        resultOutput = Text(Point(250,100),'Player 2 scores.')
        resultOutput.setFill('blue')
    else:
        resultOutput = Text(Point(250,100),'Tied round!')

    resultOutput.setStyle('bold')
    resultOutput.setSize(25)
    resultOutput.draw(win)
    win.getMouse()
    summary.undraw()
    userInputs.undraw()
    proceed.undraw()
    resultOutput.undraw()

def generateTarget(): #Target number is made
    target = random.randrange(100,250)
    return target

def generateTerms(): #Six random terms are made
    terms = [random.randrange(1, 9) for _ in range(0, 6)]
    return terms

def userTermChecker(x,y): #Checks if inputs are valid
    acceptableChar = ['+','-','*','/','(',')']
    arithmeticChar = ['+','-','*','/']
    for i in range((len(x))-1): #prevent player from catenating numbers
        if x[i] not in acceptableChar and x[i+1] not in acceptableChar:
            return 1
    for i in range((len(x))-1): #catches arithmetic error, '++' '*/' etc
        if x[i] in arithmeticChar and x[i+1] in arithmeticChar:
            return 1
    if x.count('(') != x.count(')'): #catch parenthesis imbalance
        return 1
    for item in x: #clean up for final checks
        for i in acceptableChar:
            if item == i:
                x = x.replace(item,'')
    for item in x: #check if bad characters are used or good characters are used too much
        if item not in y:
            return 1
        if x.count(item) > y.count(item):
            return 1


win = GraphWin('Number Game',500,500)
win.yUp()
makeIntro(win)
total1 = 0
total2 = 0
while total1 < 3 and total2 < 3:
    playerReady(win,'1')
    x = generateTarget()
    y = generateTerms()
    p1Out = gameBoard(win,x,y,'1')
    playerReady(win,'2')
    p2Out = gameBoard(win,x,y,'2')
    s1 = p1Out.__getitem__(0)
    s2 = p2Out.__getitem__(0)
    p1Eq = p1Out.__getitem__(1)
    p2Eq = p2Out.__getitem__(1)
    roundSummary(win,s1,s2,p1Eq,p2Eq)
    if s1 < s2:
        total1 = total1 + 1
    elif s2 < s1:
        total2 = total2 + 1
if total1 == 3:
    playerWin(win,'1')
else:
    playerWin(win,'2')